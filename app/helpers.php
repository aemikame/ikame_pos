<?php

function format_uang($angka){ 
    $hasil =  number_format($angka,0, ',' , '.'); 
    return 'Rp&nbsp;'.$hasil; 
}

function format_angka($angka){ 
    $hasil =  number_format($angka,0, ',' , '.'); 
    return $hasil; 
}