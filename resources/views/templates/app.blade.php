
<!doctype html>
<html lang="en">
  <head>
    @include('templates.head')
  </head>

  <body>

    @include('templates.header')

    <div class="container-fluid">
      <div class="row">
        <nav class="col-md-1 d-none d-md-block bg-light sidebar">
          <div class="sidebar-sticky">

          @include('templates.sidebar')
            
          </div>
        </nav>

        <main role="main" class="col-md-11 ml-sm-auto col-lg-11 px-4">
          @section('content')
            @show
        </main>
      </div>
    </div>
 
  @include('templates.footer')
  </body>
</html>
