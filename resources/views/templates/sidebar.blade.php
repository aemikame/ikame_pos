
            <a href="{{ url('/')}}">
              <div class="card mx-3 mt-2 menuutama {{ Request::path() == '/' ? 'aktif' : '' }}">
                <div class="card-body p-0 m-0 w-auto" style="text-align: center!important; text-decoration:none;">
                  <span class="mt-2" data-feather="home"></span>
                  <p class="p-0 m-0 mb-1 huruf-menuutama">Home</p>
                </div>
              </div>
            </a>

            <a href="{{ url('produk')}}">
              <div class="card mx-3 mt-2 menuutama {{ Request::path() == 'produk' ? 'aktif' : '' }}">
                <div class="card-body p-0 m-0" style="text-align: center!important; text-decoration:none;">
                  <span class="mt-2" data-feather="box"></span>
                  <p class="p-0 m-0 mb-1 huruf-menuutama">Produk</p>
                </div>
              </div>
            </a>
            
            <a href="{{ url('transaksi')}}">
              <div class="card mx-3 mt-2 menuutama {{ Request::path() == 'transaksi' ? 'aktif' : '' }}">
                <div class="card-body p-0 m-0" style="text-align: center!important; text-decoration:none;">
                  <span class="mt-2" data-feather="shopping-cart"></span>
                  <p class="p-0 m-0 mb-1 huruf-menuutama">Transaksi</p>
                </div>
              </div>
            </a>

            <a href="{{ url('pelanggan')}}">
              <div class="card mx-3 mt-2 menuutama {{ Request::path() == 'pelanggan' ? 'aktif' : '' }}">
                <div class="card-body p-0 m-0" style="text-align: center!important; text-decoration:none;">
                  <span class="mt-2" data-feather="users"></span>
                  <p class="p-0 m-0 mb-1 huruf-menuutama">Pelanggan</p>
                </div>
              </div>
            </a>
            
            <a href="{{ url('laporan')}}">
              <div class="card mx-3 mt-2 menuutama {{ Request::path() == 'laporan' ? 'aktif' : '' }}">
                <div class="card-body p-0 m-0" style="text-align: center!important; text-decoration:none;">
                  <span class="mt-2" data-feather="pie-chart"></span>
                  <p class="p-0 m-0 mb-1 huruf-menuutama">Laporan</p>
                </div>
              </div>
            </a>
            
            <a href="{{ url('pengaturan')}}">
              <div class="card mx-3 mt-2 menuutama {{ Request::path() == 'pengaturan' ? 'aktif' : '' }}">
                <div class="card-body p-0 m-0" style="text-align: center!important; text-decoration:none;">
                  <span class="mt-2" data-feather="settings"></span>
                  <p class="p-0 m-0 mb-1 huruf-menuutama">Pengaturan</p>
                </div>
              </div>
            </a>

            <a href="{{ url('pesan')}}">
              <div class="card mx-3 mt-2 menuutama {{ Request::path() == 'pesan' ? 'aktif' : '' }}">
                <div class="card-body p-0 m-0" style="text-align: center!important; text-decoration:none;">
                  <span class="mt-2" data-feather="message-square"></span>
                  <p class="p-0 m-0 mb-1 huruf-menuutama">Pesan</p>
                </div>
              </div>
            </a>