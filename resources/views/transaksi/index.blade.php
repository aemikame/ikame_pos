@extends('templates.app')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
Transaksi
</div>

<div class="card" style="width: 18rem;">
  <ul class="list-group list-group-flush">
    <a href="{{ url('transaksi/penjualan')}}"> <li class="list-group-item">Buat Transaksi Penjualan</li></a>
    <a href="#"> <li class="list-group-item">List Transaksi</li></a>
    <a href="#"> <li class="list-group-item">Draf Transaksi</li></a>
    <a href="#"> <li class="list-group-item">Buat Transaksi Pembelian</li></a>
  </ul>
</div>

@endsection