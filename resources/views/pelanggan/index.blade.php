@extends('templates.app')

@section('content')

<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3">

<div class="btn-toolbar mb-2 mb-md-0">
  <form class="form-inline">
    <input class="form-control mr-sm-2 form-control-sm" type="search" placeholder="Cari data pelanggan..." aria-label="cari">
    <button class="btn btn-sm form-control-sm btn-outline-default my-1 my-sm-0 " type="submit">Cari</button>
  </form>
</div>

<div class="btn-toolbar mb-2 mb-md-0">
              <div class="btn-group mr-2">
                <button class="btn btn-sm btn-outline-secondary">Export PDF</button>
                <button class="btn btn-sm btn-outline-secondary">Export Excel</button>
              </div>
            </div>
</div>

 <h5>Data Pelanggan</h5>
          <div class="table-responsive">
            <table class="table table-striped table-sm">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nama</th>
                  <th>No Hp</th>
                  <th>Alamat</th>
                  <th>Terakhir Order</th>
                  <th>aksi</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1,001</td>
                  <td>Lorem</td>
                  <td>ipsum</td>
                  <td>dolor</td>
                  <td>sit</td>
                  <td>view | edit | delete</td>
                </tr>
                <tr>
                  <td>1,002</td>
                  <td>amet</td>
                  <td>consectetur</td>
                  <td>adipiscing</td>
                  <td>elit</td>
                  <td>view | edit | delete</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>Integer</td>
                  <td>nec</td>
                  <td>odio</td>
                  <td>Praesent</td>
                  <td>view | edit | delete</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>libero</td>
                  <td>Sed</td>
                  <td>cursus</td>
                  <td>ante</td>
                  <td>view | edit | delete</td>
                </tr>
            </tbody>
        </table>
  <nav aria-label="Page navigation example">
    <ul class="pagination">
      <li class="page-item"><a class="page-link" href="#">Previous</a></li>
      <li class="page-item"><a class="page-link" href="#">1</a></li>
      <li class="page-item"><a class="page-link" href="#">2</a></li>
      <li class="page-item"><a class="page-link" href="#">3</a></li>
      <li class="page-item"><a class="page-link" href="#">Next</a></li>
    </ul>
  </nav>
@endsection