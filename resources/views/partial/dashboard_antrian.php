<div class="card-group">
  
  
  <div class="card">
    <div class="card-body">
      <h5 class="card-title"><span class="badge badge-pill badge-dark">1</span> Pesanan Masuk </h5>
        <ul class="list-group list-group-flush">
          <li class="list-group-item">Cras justo odio</li>
          <li class="list-group-item">Dapibus ac facilisis in</li>
          <li class="list-group-item">Morbi leo risus</li>
          <li class="list-group-item">Porta ac consectetur ac</li>
          <li class="list-group-item">Vestibulum at eros</li>
        </ul> 
    </div>
    <div class="card-footer">
      <small class="text-muted">Terakhir update 5 menit lalu</small>
    </div>
  </div>

  <div class="card">
    <div class="card-body">
      <h5 class="card-title"><span class="badge badge-pill badge-dark">2</span> Bayaran Lunas </h5>
      <ul class="list-group list-group-flush">

          <li class="list-group-item">Porta ac consectetur ac</li>
          <li class="list-group-item">Vestibulum at eros</li>
        </ul> 
    </div>
    <div class="card-footer">
      <small class="text-muted">Terakhir update 5 menit lalu</small>
    </div>
  </div>

  <div class="card">
    <div class="card-body">
      <h5 class="card-title"><span class="badge badge-pill badge-dark">3</span> Barang disiapkan</h5>
      <ul class="list-group list-group-flush">
          <li class="list-group-item">Cras justo odio</li>
          <li class="list-group-item">Dapibus ac facilisis in</li>
          <li class="list-group-item">Morbi leo risus</li>
        </ul> 
    </div>
    <div class="card-footer">
      <small class="text-muted">Terakhir update 5 menit lalu</small>
    </div>
  </div>

  <div class="card">
    <div class="card-body">
      <h5 class="card-title"><span class="badge badge-pill badge-dark">4</span> Barang Dikirim</h5>
      <ul class="list-group list-group-flush">
          <li class="list-group-item">Cras justo odio</li>
          <li class="list-group-item">Dapibus ac facilisis in</li>
          <li class="list-group-item">Morbi leo risus</li>
          <li class="list-group-item">Porta ac consectetur ac</li>
        </ul> 
    </div>
    <div class="card-footer">
      <small class="text-muted">Terakhir update 5 menit lalu</small>
    </div>
  </div>


  
</div>