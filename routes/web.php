<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('templates.home');
});

Route::get('laporan', function () {
    return view('laporan.index');
});

Route::get('pelanggan', function () {
    return view('pelanggan.index');
});

Route::get('produk', function () {
    return view('produk.index');
});

// Transaksi
Route::get('transaksi', function () {
    return view('transaksi.index');
});

Route::get('transaksi/penjualan/', function () {
    return view('penjualan.index');
});


//

Route::get('pengaturan', function () {
    return view('pengaturan.index');
});

Route::get('pesan', function () {
    return view('pesan.index');
});


